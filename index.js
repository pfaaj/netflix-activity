// DOM element where the Timeline will be attached
var container = document.getElementById('visualization');
let omdb = "http://www.omdbapi.com/?t={0}&apikey=58ed9d8e";

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
        ;
    });
  };
}


get_unique_titles = (data) => {
  let unique_titles = new Set();

  //shorten the content
  data.forEach((item) => {

    unique_titles.add(item["content"]);

  });
  return unique_titles;
};

convert_date = (data) => {
  //convert the date
  data.forEach((item) => {

    var from = item["start"];
    var temp = from.split("/");
    var to = temp[2] + "-" + temp[1] + "-" + temp[0];
    item["start"] = to;
  });
};




convert_date(netflix_data);

//shorten the content
netflix_data.forEach((item) => {

  var title = item["content"];
  var elements = title.split(":");
  item["content"] = elements[0];


});

titles = get_unique_titles(netflix_data);

let total_duration = 0;
var duration_div = document.getElementById('duration');

var promises = [];

let titles_array = [...titles];

let durations = [];

titles_array.forEach((item) => {

  promises.push(fetch(omdb.format(item))
    .then(function (response) {
      return response.json();
    })
    .then(function (metadata) {
      let duration = 0;
      if (!!metadata["Runtime"]) {
        duration = parseInt(metadata["Runtime"].split(" ")[0], 10);

      }
      //console.log({'title': item, 'duration': duration});
      let key = item;
      let obj = {};
      obj[key] = duration;
      durations.push(obj); 
      return { 'title': item, 'duration': duration };


    }));
});

//computing the duration incorrectly (need to compute for every episode!)


Promise.all(promises.map(p => p.catch(() => undefined))).then(values => {
  // returned data is in arguments[0], arguments[1], ... arguments[n]
  // you can process it here
  let duration = 0;
  console.log("Data " + JSON.stringify(netflix_data));

  values.forEach(item => {
    if (typeof item !== "undefined" && !isNaN(item['duration'])) {

     netflix_data.slice(0,228).forEach(title => {
        if(item["title"] === title["content"]){
          duration += item['duration'];
          console.log("Item: {0} Id: {1} Duration: {2}".format(item['title'], title["id"], duration));
        }   
     });
    }
      
    

  });
  let hours = Math.floor(duration / 60);
  console.log("Total time watched {0} hours".format(hours));
  duration_div.innerText += "Total time watched {0} hours".format(hours);
});

console.log("Total duration {0}".format(total_duration));

//only data for 2019
var items = new vis.DataSet(netflix_data.slice(1, 228));


// Configuration for the Timeline
var options = {};

// Create a Timeline
var timeline = new vis.Timeline(container, items, options);